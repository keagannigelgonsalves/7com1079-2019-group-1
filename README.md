---
module: 7COM1079
subtitle: '7COM1079 -- Team Research and Development Project'
title: ' # 7COM1079-2019 Group-1 coursework'
---
1. Group Number: 1
2. Group Members: 
Member 1: Keagan Nigel Gonsalves, 
Member 2: Constantin Apetrei
Member 3: Akhalekpenkhian Esangbedo
Member 4: Z Dang Manh Cuong 
Member 5: Deria Ali.
3. Purpose of this repository: This repositiory is solely for the completion of the coursework for the course 
"Team Research and Development", we will be looking into the dataset on India population and literacy to identify
if there is any correlation or significance due to the total population in each state. we will be investigating 
the research question if there is a coreelation between the outcome of the total nuimber of graduates by the total population
in each state.
